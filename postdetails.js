database = firebase.database();
var postId = window.location.toString().split('=')[1];
var container = document.getElementById('container');
var loader = document.getElementById("loader");
var commentsCounter = document.getElementById("comments-counter");
var postDateLabel = document.getElementById("date");
var postTextLabel = document.getElementById("post-text");
var postAuthorLabel = document.getElementById("post-author");
var detailsMap = document.getElementById("details-map");
var addressLabel = document.getElementById("address");
var visitCounterLabel = document.getElementById("visit-counter");
var commentForm = document.getElementById("comment-form");
var addCommentFormButton = document.getElementById("add-comment-form-button");
var captchaInput = document.getElementById("captcha-input");
var captchaLabel = document.getElementById("captcha-label");
var captchaMinCode = 100;
var captchaMaxCode = 999;
var captchaCode = Math.floor((Math.random() * captchaMaxCode) + captchaMinCode);
var commentTextField = document.getElementById("comment-text");
var commentAuthorField = document.getElementById("comment-author");
var commentErrorLabel = document.getElementById("comment-error-label");
var lat = 52.069167;
var lon = 19.480556;
loadDetails(postId);

function loadDetails(id) {
    database.ref("posts/" + id).once("value").then(function(snapshot) {
        var dateString = snapshot.val().date;
        var text = snapshot.val().text;
        var author = snapshot.val().author;
        var locationChecked = snapshot.val().locationChecked;
        var address = snapshot.val().address;
        var comments = snapshot.val().comments;
        var visitCounter = snapshot.val().visitCounter + 1;
        if (comments != null) {
            for (var i = 0; i < comments.length; i++) {
                showComment(i);
                if (i == comments.length - 1) {
                    loader.style.display = "none";
                }
            }
            commentsCounter.innerHTML = "Odpowiedzi: <span class='prime-color'>" + comments.length + "</span>, ";
        } else {
            commentsCounter.innerHTML = "Odpowiedzi: <span class='prime-color'>0</span>, ";
            loader.style.display = "none";
        }
        postDateLabel.innerHTML = dateString;
        postTextLabel.innerHTML = "Pytanie #" + id + ": <span class='post-text'>" + text + "</span>";
        postAuthorLabel.innerHTML = "Od: <span class='prime-color'>" + author + "</span>";
        if (locationChecked) {
            detailsMap.style.display = "block";
            lat = snapshot.val().lat;
            lon = snapshot.val().lon;
            addressLabel.innerHTML = "Adres: <i class='prime-color'>" + address + "</i>";
            initMap();
        }
        if (address == undefined) {
            addressLabel.innerHTML = "Brak lokalizacji";
        }
        visitCounterLabel.innerHTML = "Wyświetlenia: <span class='prime-color'>" + visitCounter + "</span>";
        updateCounter(visitCounter);
    })
}

function updateCounter(count) {
    database.ref("posts/" + postId).update({
        visitCounter: count
    });
}

function initMap() {
    var point = {lat: lat, lng: lon};
    var map = new google.maps.Map(detailsMap, {
        zoom: 16,
        center: point
    });
    var marker = new google.maps.Marker({
      map: map,
      position: point
    });
}

function showComment(commentId) {
    database.ref('posts/' + postId + '/comments/' + commentId).once('value').then(function(snapshot) {
        var date = snapshot.val().date;
        var text = snapshot.val().text;
        var author = snapshot.val().author;
        var element = document.createElement('div');
        element.className = 'post';
        element.innerHTML = commentHTML(commentId, date, text, author);
        container.insertBefore(element, container.childNodes[8]);
    })
}

function openCommentForm() {
    if (commentForm.style.display === "block") {
        commentForm.style.display = "none";
        addCommentFormButton.innerHTML = "Dodaj odpowiedź"
    } else {
        commentForm.style.display = "block";
        addCommentFormButton.innerHTML = "Anuluj odpowiedź"
        captchaCode = Math.floor((Math.random() * captchaMaxCode) + captchaMinCode);
        captchaLabel.innerHTML = "Kod captcha: " + captchaCode;
    }
}

function addComment() {
    if (commentTextField.value != "" && commentAuthorField.value != "" && captchaInput.value == captchaCode) {
        saveCommentInDatabase();
    } else {
        commentErrorLabel.style.display = "inline";
    }
}

function saveCommentInDatabase() {
    var date = new Date();
    var dateString = createDateString(date);
    database.ref("posts/" + postId + "/comments").once("value").then(function(snapshot) {
        var commentId = 0;
        if (snapshot.val() != null) {
            commentId = snapshot.val().length;
        }
        database.ref('posts/' + postId + '/comments/' + commentId).set({
            text: commentTextField.value,
            author: commentAuthorField.value,
            date: dateString
        });
        clearValues();
        commentsCounter.innerHTML = "Odpowiedzi: <span class='prime-color'>" + (commentId + 1) + "</span>, ";
        showComment(commentId);
    })
}

function clearValues() {
    openCommentForm();
    commentTextField.value = "";
    commentAuthorField.value = "";
    captchaInput.value = "";
    commentErrorLabel.style.display = "none";
}

function createDateString(date) {
    var day = addZeroIfLessThanTen(date.getDate())
    var month = addZeroIfLessThanTen(date.getMonth())
    var year = date.getFullYear()
    var hours = addZeroIfLessThanTen(date.getHours())
    var minutes = addZeroIfLessThanTen(date.getMinutes())
    return day + "/" + month + "/" + year + " " + hours + ":" + minutes;
}

function addZeroIfLessThanTen(number) {
    if (number < 10) { number = "0" + number; }
    return number;
}

function commentHTML(id, date, text, author) {
    id = id + 1;
    var dateDiv = "<div class='date'>" + date + "</div>";
    var textLabel = "Odpowiedź #" + id + ": <span class='post-text'>" + text + "</span><br>";
    var authorLabel = "Od: <span class='prime-color'>" + author + "</span>";
    return dateDiv + textLabel + authorLabel;
}
