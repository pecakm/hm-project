var database = firebase.database();
var container = document.getElementById("container");
var addPostForm = document.getElementById("add-post-form");
var addPostFormButton = document.getElementById("add-post-form-button");
var checkbox = document.getElementById("address-checkbox");
var localizationForm = document.getElementById("localization-form");
var searchAddressButton = document.getElementById("search-address-button");
var addressInput = document.getElementById("address-input");
var loader = document.getElementById("loader");
var captchaLabel = document.getElementById("captcha-label");
var captchaInput = document.getElementById("captcha-input");
var captchaMinCode = 100;
var captchaMaxCode = 999;
var captchaCode = Math.floor((Math.random() * captchaMaxCode) + captchaMinCode);
var textField = document.getElementById("add-post-textarea");
var authorField = document.getElementById("post-author");
var postErrorLabel = document.getElementById("post-error-label");
var lon = 0.0;
var lat = 0.0;
var addressString = "";
var dateString = "";
updateVisitCounter();
loadPosts()

function updateVisitCounter() {
    database.ref("visitCounter/").once("value").then(function(snapshot) {
        var value = 1;
        if (snapshot.val() != null) {
            value = snapshot.val().value + 1;
        }
        database.ref("visitCounter/").update({
            value: value
        });
    })
}

function showForm() {
    if (addPostForm.style.display === "block") {
        addPostForm.style.display = "none";
        addPostFormButton.innerHTML = "Dodaj pytanie"
    } else {
        addPostForm.style.display = "block";
        addPostFormButton.innerHTML = "Anuluj pytanie";
        captchaCode = Math.floor((Math.random() * captchaMaxCode) + captchaMinCode);
        captchaLabel.innerHTML = "Kod captcha: " + captchaCode;
        initMap();
    }
}

function checkboxChanged() {
    if (checkbox.checked) {
        localizationForm.style.pointerEvents = "all";
        localizationForm.style.opacity = 1;
    } else {
        localizationForm.style.pointerEvents = "none";
        localizationForm.style.opacity = 0.4;
    }
}

function initMap() {
    var poland = {lat: 52.069167, lng: 19.480556};
    var map = new google.maps.Map(document.getElementById('add-post-map'), {
        zoom: 6,
        center: poland
    });
    var geocoder = new google.maps.Geocoder();
    searchAddressButton.addEventListener('click', function() {
        geocodeAddress(geocoder, map);
    });
}

function geocodeAddress(geocoder, resultsMap) {
    geocoder.geocode({'address': addressInput.value}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            resultsMap.setZoom(16);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
            });
            addressInput.value = addressString = results[0].formatted_address;
            lat = results[0].geometry.location.lat();
            lon = results[0].geometry.location.lng();
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}

function loadPosts() {
    database.ref('posts/').once('value').then(function(snapshot) {
        if (snapshot.val() != null) {
            for (var i = 0; i < snapshot.val().length; i++) {
                showPost(i);
                if (i == snapshot.val().length - 1) {
                    loader.style.display = "none";
                }
            }
        } else {
            loader.style.display = none;
        }
    });
}

function showPost(id) {
    database.ref('posts/' + id).once('value').then(function(snapshot) {
        var date = snapshot.val().date;
        var text = snapshot.val().text;
        var author = snapshot.val().author;
        var locationChecked = snapshot.val().locationChecked;
        var address = snapshot.val().address;
        var commentsCounter = 0;
        var visitCounter = snapshot.val().visitCounter;
        if (snapshot.val().comments != null) {
            commentsCounter = snapshot.val().comments.length;
        }
        var element = document.createElement('div');
        element.className = 'post';
        element.innerHTML = postHTML(id, date, text, author, locationChecked, address, commentsCounter, visitCounter);
        container.insertBefore(element, container.childNodes[4]);
    })
}

function postHTML(id, date, text, author, locationChecked, address, commentsCounter, visitCounter) {
    var dateDiv = "<div class='date'>" + date + "</div>";
    var postIdText = "<br>Pytanie #" + id + ": ";
    var postText = "<a class='post-text' href='postdetails.html?source=" + id + "'>" + text + "</a><br><br>";
    authorText = "od: <a class='prime-color'>" + author + "</a>, ";
    var localizationText = "";
    if (locationChecked == false) {
        addressText = "w: <i>Brak lokalizacji</i>";
    } else {
        addressText = "w: <i class='prime-color'>" + address + "</i>";
    }
    var commentsCounterText = "<br>Odpowiedzi: <span class='prime-color'>" + commentsCounter + "</span>, ";
    var visitCounterText = "Wyświetlenia: <span class='prime-color'>" + visitCounter + "</span>";
    return dateDiv + postIdText + postText + authorText + addressText + commentsCounterText + visitCounterText;
}

function addPost() {
    var date = new Date();
    dateString = createDateString(date);
    if (textField.value != "" && authorField.value != "" && captchaInput.value == captchaCode) {
        database.ref('posts/').once('value').then(function(snapshot) {
            var id = 0;
            if (snapshot.val() != null) {
                id = snapshot.val().length;
            }
            if (checkbox.checked && addressString != "") {
                savePostWithLocalization(id);
            } else {
                savePostWithoutLocalization(id);
            }
            resetValues();
            showForm();
            showPost(id);
        })
    } else {
        postErrorLabel.style.display = "inline";
    }
}

function savePostWithLocalization(id) {
    database.ref('posts/' + id).set({
        date: dateString,
        text: textField.value,
        author: authorField.value,
        locationChecked: true,
        address: addressString,
        lat: lat,
        lon: lon,
        visitCounter: 0
    });
}

function savePostWithoutLocalization(id) {
    database.ref('posts/' + id).set({
        date: dateString,
        text: textField.value,
        author: authorField.value,
        locationChecked: false,
        visitCounter: 0
    });
}

function resetValues() {
    textField.value = "";
    authorField.value = "";
    checkbox.checked = false;
    addressInput.value = "";
    captchaInput.value = "";
    postErrorLabel.style.display = "none";
}

function createDateString(date) {
    var day = addZeroIfLessThanTen(date.getDate())
    var month = addZeroIfLessThanTen(date.getMonth())
    var year = date.getFullYear()
    var hours = addZeroIfLessThanTen(date.getHours())
    var minutes = addZeroIfLessThanTen(date.getMinutes())
    return day + "/" + month + "/" + year + " " + hours + ":" + minutes;
}

function addZeroIfLessThanTen(number) {
    if (number < 10) { number = "0" + number; }
    return number
}
